export type RootStackParamList = {
  Home: undefined;
  LeaderBoard: undefined;
};



export interface LeaderBoardData {
  playerName: string;
  score: number;
  date: string;
}

export interface QuestionData {
  id: number;
  question: string;
  answers: string[];
  correctAnswer: string;
}