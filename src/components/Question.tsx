import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

interface QuestionProps {
  id: number;
  question: string;
  answers: string[];
  selectedAnswer: string;
  onSelectAnswer: (answer: string) => void;
}

const Question: React.FC<QuestionProps> = ({id, question, answers, selectedAnswer, onSelectAnswer }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.question}>{question}</Text>
      {answers.map((answer, index) => (
        <TouchableOpacity
          key={index}
          style={[styles.answerButton, selectedAnswer === answer && styles.selectedAnswerButton]}
          onPress={() => onSelectAnswer(answer)}
        >
          <Text style={styles.answerText}>{answer}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  question: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  answerButton: {
    padding: 10,
    backgroundColor: '#f0f0f0',
    borderRadius: 5,
    marginBottom: 10,
  },
  selectedAnswerButton: {
    backgroundColor: '#d0d0d0',
  },
  answerText: {
    fontSize: 16,
  },
});

export default Question;
