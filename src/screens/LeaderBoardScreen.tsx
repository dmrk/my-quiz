import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { RouteProp } from '@react-navigation/native';
import { LeaderBoardData, RootStackParamList } from '../types/types';
import { StackNavigationProp } from '@react-navigation/stack';

type LeaderBoardScreenRouteProp = RouteProp<RootStackParamList, 'LeaderBoard'>;
type LeaderBoardScreenNavigationProp = StackNavigationProp<RootStackParamList, 'LeaderBoard'>;

type Props = {
  route: LeaderBoardScreenRouteProp;
  navigation: LeaderBoardScreenNavigationProp;
};



const LeaderBoardScreen: React.FC<Props> = () => {
  const [scores, setScores] = useState<LeaderBoardData[]>([]);
  const [tapCount, setTapCount] = useState<number>(0);

  useEffect(() => {
    const loadScores = async () => {
      try {
        const existingEntries = await AsyncStorage.getItem('leaderBoard');
        if (existingEntries) {
          const parsedEntries = JSON.parse(existingEntries) as LeaderBoardData[];
          setScores(parsedEntries);
        }
      } catch (error) {
        console.error("Failed to load data", error);
      }
    };

    loadScores();
  }, []);

  useEffect(() => {
    if (tapCount === 3) {
      Alert.alert(
        "Clear Leader Board",
        "Are you sure you want to clear the leader board?",
        [
          {
            text: "Cancel",
            onPress: () => setTapCount(0),
            style: "cancel"
          },
          { text: "OK", onPress: clearLeaderBoard }
        ],
        { cancelable: false }
      );
    }
  }, [tapCount]);

  const handleTitlePress = () => {
    setTapCount(prevCount => prevCount + 1);
    setTimeout(() => setTapCount(0), 1000);
  };

  const clearLeaderBoard = async () => {
    try {
      await AsyncStorage.removeItem('leaderBoard');
      setScores([]);
      Alert.alert("Leader Board cleared successfully!");
    } catch (error) {
      console.error("Failed to clear the leader board", error);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleTitlePress}>
        <Text style={styles.title}>Leader Board</Text>
      </TouchableOpacity>
      <FlatList
        data={scores.sort((a, b) => b.score - a.score)}
        renderItem={({ item, index }) => (
          <View style={styles.scoreContainer}>
            <Text style={styles.rank}>#{index + 1}</Text>
            <Text style={styles.playerName}>{item.playerName}</Text>
            <Text style={styles.scoreText}>{item.score}</Text>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5f5f5',
    padding: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  scoreContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 10,
    marginVertical: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 3,
  },
  rank: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  playerName: {
    fontSize: 18,
    flex: 1,
    marginLeft: 10,
  },
  scoreText: {
    fontSize: 20,
  },
});

export default LeaderBoardScreen;
