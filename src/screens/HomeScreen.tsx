import React, { useState, useCallback, useRef } from 'react';
import { View, Text, TextInput, StyleSheet, FlatList, Alert, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import Question from '../components/Question';
import questionData from '../data/questions.json';
import { LeaderBoardData, RootStackParamList, QuestionData } from '../types/types';
import { StackNavigationProp } from '@react-navigation/stack';

type HomeScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Home'>;

const shuffleArray = (array: any[]) => {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
  return array;
};

const HomeScreen: React.FC = () => {
  const [playerName, setPlayerName] = useState<string>('');
  const [questions, setQuestions] = useState<QuestionData[]>([]);
  const [answers, setAnswers] = useState<{ [key: string]: string }>({});
  const [score, setScore] = useState<number>(0);
  const [nameSubmitted, setNameSubmitted] = useState<boolean>(false);
  const flatListRef = useRef<FlatList>(null);
  const navigation = useNavigation<HomeScreenNavigationProp>();

  useFocusEffect(
    useCallback(() => {
      setPlayerName('');
      setAnswers({});
      setNameSubmitted(false);
    }, [])
  );

  const handleSelectAnswer = (questionId: string, answer: string, index: number) => {
    setAnswers(prevAnswers => ({
      ...prevAnswers,
      [questionId]: answer
    }));
    if (flatListRef.current && index < questions.length - 1) {
      flatListRef.current.scrollToIndex({ animated: true, index: index + 1 });
    }
  };

  const handleSubmitName = () => {
    if (playerName.trim() === '') {
      Alert.alert("Name Required", "Please enter your name.");
      return;
    }
    setNameSubmitted(true);
    const shuffledQuestions = shuffleArray(questionData).slice(0, 20);
    const questionsWithShuffledAnswers = shuffledQuestions.map(question => ({
      ...question,
      answers: shuffleArray(question.answers)
    }));
    setQuestions(questionsWithShuffledAnswers);
  };

  const handleSubmit = async () => {
    const unansweredQuestionIndex = questions.findIndex(question => !answers[question.id]);

    if (unansweredQuestionIndex !== -1) {
      if (flatListRef.current) {
        flatListRef.current.scrollToIndex({ animated: true, index: unansweredQuestionIndex });
      }
      Alert.alert("Incomplete", "Please answer all questions before submitting.");
      return;
    }

    let newScore: number = 0;
    questions.forEach(question => {
      if (answers[question.id] === question.correctAnswer) {
        newScore += 1;
      }
    });
    setScore(newScore);

    const newEntry : LeaderBoardData = {
      playerName: playerName,
      score: newScore,
      date: new Date().toISOString(),
    };

    try {
      const existingEntries = await AsyncStorage.getItem('leaderBoard');
      const leaderBoardData: LeaderBoardData[] = existingEntries ? JSON.parse(existingEntries) : [];
      const newLeaderBoardData = leaderBoardData.filter(u => u.playerName !== playerName);
      newLeaderBoardData.push(newEntry);
      await AsyncStorage.setItem('leaderBoard', JSON.stringify(newLeaderBoardData));
    } catch (error) {
      console.error("Failed to save data to the storage", error);
    }

    Alert.alert("Quiz Finished", `Your score is ${newScore}`, [
      { text: "OK", onPress: () => navigation.navigate('LeaderBoard') }
    ]);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.leaderboardLink} onPress={() => navigation.navigate('LeaderBoard')}>
        <Text style={styles.leaderboardLinkText}>{"Go to leaderboard >"}</Text>
      </TouchableOpacity>
      {!nameSubmitted ? (
        <View style={styles.inputContainer}>
          <Text style={styles.title}>Enter Your Name</Text>
          <TextInput
            style={styles.input}
            placeholder="Enter your name"
            value={playerName}
            onChangeText={setPlayerName}
          />
          <TouchableOpacity style={styles.button} onPress={handleSubmitName}>
            <Text style={styles.buttonText}>Start Quiz!</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <>
          <FlatList
            ref={flatListRef}
            data={questions}
            renderItem={({ item, index }) => (
              <Question
                id={item.id}
                question={item.question}
                answers={item.answers}
                selectedAnswer={answers[item.id] || ''}
                onSelectAnswer={(answer: string) => handleSelectAnswer(item.id, answer, index)}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
          />
          <TouchableOpacity style={styles.button} onPress={handleSubmit} disabled={!playerName || Object.keys(answers).length !== questions.length}>
            <Text style={styles.buttonText}>Submit</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#f5f5f5',
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    height: 50,
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 20,
    paddingHorizontal: 10,
    width: '80%',
    backgroundColor: '#fff',
  },
  button: {
    backgroundColor: '#28a745',
    paddingVertical: 15,
    paddingHorizontal: 40,
    borderRadius: 8,
    marginBottom: 20,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  leaderboardLink: {
    position: 'absolute',
    top: 40,
    right: 20,
  },
  leaderboardLinkText: {
    color: '#007bff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  score: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 20,
  },
});

export default HomeScreen;
